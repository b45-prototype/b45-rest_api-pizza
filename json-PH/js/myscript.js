function mn (){
    $.getJSON('data/pizza.json', function (data){
        let menu = data.menu;
        $('#list-menu').empty();
        $.each(menu, function (i,dt){
            $('#list-menu').append('<div class="col-md-4"><div class="card mb-3"><img src="img/menu/'+dt.gambar+'" class="card-img-top" alt="..."><div class="card-body"><h5 class="card-title">'+dt.nama+'</h5><p class="card-text">'+dt.deskripsi+'</p><a href="#" class="btn btn-primary">RP. '+dt.harga+',-</a></div></div></div>')});
    });
}

mn();
// menangkap class nav-link dan 
// menjalankan aksi jika class tsb di klik
$('.nav-link').on('click', function (){
    // hilangkan semua class active 
    $('.nav-link').removeClass('active');
    // berikan class active pada element tertentu
    $(this).addClass('active');

    let category = $(this).html();
    $('hi').html(category);

    if (category == 'All Menu'){
        $('list-menu').html('');
        mn();
        return;
    }

    $.getJSON('data/pizza.json', function (data){
        let menu = data.menu;
        let content = '';

        $.each(menu, function (i,dt){
            if (dt.kategori == category.toLowerCase()){
                content += '<div class="col-md-4"><div class="card mb-3"><img src="img/menu/'+dt.gambar+'" class="card-img-top" alt="..."><div class="card-body"><h5 class="card-title">'+dt.nama+'</h5><p class="card-text">'+dt.deskripsi+'</p><a href="#" class="btn btn-primary">RP. '+dt.harga+',-</a></div></div></div>';
            }
        });

    $('#list-menu').html(content);
    });
    return;
});
<?php 
    // contoh array
    // $mahasiswa = 
    // [
    //     [
    //     "nama"=> "Basofi S",
    //     "umur"=> 23,
    //     "NIM"=> "135150207111070",
    //     "lulus"=> true
    //     ],
    //     [
    //     "nama"=> "sdfsdf",
    //     "umur"=> 22,
    //     "NIM"=> "1231231231",
    //     "lulus"=> false
    //     ]
    // ];

    // koneksi ke database dengan PDO
    // buat handler
    $dbh = new PDO('mysql:host=localhost;dbname=phpmvc','root',null,null);
    // konek de database dengan membuat query database terlebih dahulu
    $db = $dbh->prepare('SELECT * FROM mahasiswa');
    // menjalankan query
    // sebaiknya membuat class PDO terlebih dahulu
    $db->execute();
    // mengambil semua data mahasiswa dengan return value asosiatif
    $mahasiswa = $db->fetchAll(PDO::FETCH_ASSOC);


    $data = json_encode($mahasiswa);
    echo $data;

?>
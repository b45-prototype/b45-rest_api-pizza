// contoh penerapan data object to JSON
// let mahasiswa = {
//     nama : "Achmad Dinda",
//     NIM : "135150207111070",
//     email : "b45@dr.com"
// }
// console.log(JSON.stringify(mahasiswa));

// contoh penerapan data JSON ke object
// menggunakan vanila js / manual js
// let xhr = new XMLHttpRequest();
// xhr.onreadystatechange = function ()
// {
//     // ketika AJAX sudah siap 
//     if (xhr.readyState == 4 && xhr.status){
//         // data mahasiswa berupa apapun respon dari AJAX
//         // yang awalnya String JSON dirubah ke data object
//         let mahasiswa = JSON.parse(this.responseText);
//         console.log(mahasiswa);
//     }
// }
// // untuk menjalankan AJAX
// xhr.open('GET', 'coba.json',true);
// xhr.send();

// contoh penerapan data JSON ke object
// menggunakan JQUERY
$.getJSON('coba.json', function (data){
    console.log(data);
});